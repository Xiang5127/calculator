function convertArray(str){
    //convert the input into an array according to numbers and operators
    let array = [];
    let num = "";    
    for(let i=0; i<str.length; i++){
        //current data is an operator
        if(str.charAt(i) == '+' || str.charAt(i) == '-' || str.charAt(i) == '/' || str.charAt(i) == '*'){
            array.push(str.charAt(i));                        
        }else{
            //current data is a number
            do {
                //this do while loop is to get all digits of a number stored into num variable
                num += str.charAt(i);
                //searching through the number
                i++;
            } while (i<str.length && str.charAt(i) !== '+' && str.charAt(i) !== '-' && str.charAt(i) !== '/' && str.charAt(i) !== '*');
            //push num into array
            array.push(num);
            //reset num
            num = "";
            //decrement the extra i
            i--;
        }        
    }return array;
}

function output(input){
    //convert the input into an array according to numbers and operators
    strArray = convertArray(input);
}

console.log(convertArray("10+2-30*500*32+9/200+3000*135/4222/42222-9000+3*22*22*22/6644+243424"));


